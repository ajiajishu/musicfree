## MusicFree

github：[MusicFree](https://github.com/maotoumao/MusicFree)

**本仓库仅作为备份，个人以及群友使用。**

- 感谢作者的无私开源项目
- 感谢网络热心网友制作音乐源
- 适配PC和安卓

## 下载

[安卓端](https://www.123pan.com/s/grz2jv-JPkAA.html)

[PC端](https://www.123pan.com/s/grz2jv-IPkAA.html)

下载后安装插件
打开软件》插件管理》网络安装，复制本仓库链接即可或原链接也行，本仓库将以下链接全部放在了一起，方便使用。

QQ音乐

http://adad23u.appinstall.life/dist/qq/index.js

网易云音乐

https://gitee.com/raycn120/musicfree/raw/master/netease.js

酷我音乐

http://adad23u.appinstall.life/dist/kuwo/index.js

咪咕音乐

http://adad23u.appinstall.life/dist/migu/index.js

Bilibili

http://adad23u.appinstall.life/dist/bilibili/index.js

综合插件（集合多个音乐源）

https://gitlab.com/acoolbook/musicfree/-/raw/main/music.json

备用集合音源

https://gitee.com/maotoumao/MusicFreePlugins/raw/master/plugins.json

排行榜插件

https://gitee.com/maotoumao/MusicFreePlugins/raw/v0.1/dist/suno/index.js

排行榜插件

https://gitee.com/maotoumao/MusicFreePlugins/raw/v0.1/dist/udio/index.js

## 操作示例

<img src="https://telegraph-image-7qb.pages.dev/file/b48e25154a15fe7604d66.jpg"/>


## 后缀

下载好音乐后需要改后缀不然只能在这个软件内听歌
下载好音乐后打开下载目录是这样子的：

可爱女人-周杰伦.apk&type$convert_url_with_sign
然后修改成：可爱女人-周杰伦.MP3

## 更新与提示：
如果VIP的歌听不了，点开插件页面，然后找到你无法播放的源点击更新即可。